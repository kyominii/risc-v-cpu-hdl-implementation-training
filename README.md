# RISC-V CPU Implementation Training

The idea behind this repository is to keep track of my implementation of the RISC-V processor architecture with TL-Verilog.

This code is produced as part of the course I followed: [Building a RISC-V CPU Core](https://www.edx.org/course/building-a-risc-v-cpu-core) on edX.
